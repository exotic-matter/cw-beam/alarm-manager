{{ objname | escape | underline}}

.. rubric:: Description

.. currentmodule:: {{ module }}

.. autoclass:: {{ objname }}

   {% block attributes %}
   {% if attributes %}
   .. rubric:: Attributes
   .. autosummary::
      :nosignatures:
      :toctree: {{ objname }}
   {% for item in attributes %}
   {%- if item not in inherited_members %}
      {{ name }}.{{ item }}
   {%- endif %}
   {% endfor %}
   {% endif %}
   {% endblock %}

   {% block methods %}
   {% if methods %}
   .. rubric:: Methods
   .. autosummary::
      :nosignatures:
      :toctree: {{ objname }}
   {% for item in methods %}
   {%- if item not in inherited_members %}
      {{ name }}.{{ item }}
   {%- endif %}
   {% endfor %}
   {% endif %}
   {% endblock %}