Welcome to the Alarm Manager Documentation!
===========================================

.. image:: https://badge.fury.io/py/alarm-manager.svg
    :target: https://badge.fury.io/py/alarm-manager
    :alt: PyPI Latest Version

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/badges/master/pipeline.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/-/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/badges/master/coverage.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/alarm-manager/badge/?version=latest
    :target: https://alarm-manager.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License: GPL v3


:obj:`alarm_manager` is ...

If this is your first time using :obj:`alarm_manager`, have a look at our :doc:`Readme<project/README>` for a more
detailed summary and installation instructions. If you're already familiar with this package, or you want to dive
straight in, you can jump to the :doc:`API reference <api>`.


.. toctree::
    :maxdepth: 1

    Welcome <self>
    Readme <project/README.md>
    api
    Changelog <project/CHANGELOG.md>
    Contributing <project/CONTRIBUTING.md>
    License <project/LICENSE.md>
