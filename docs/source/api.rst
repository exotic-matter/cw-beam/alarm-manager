API Reference
=============

.. rubric:: Description
.. automodule:: alarm_manager
.. currentmodule:: alarm_manager

.. rubric:: Modules
.. autosummary::
    :toctree: api