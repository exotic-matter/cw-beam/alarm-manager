# README

[![PyPI Latest Version](https://badge.fury.io/py/alarm-manager.svg)](https://badge.fury.io/py/alarm-manager)
[![pipeline status](https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/badges/master/pipeline.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/-/commits/master)
[![coverage report](https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/badges/master/coverage.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/alarm-manager/badge/?version=latest)](https://alarm-manager.readthedocs.io/en/latest/?badge=latest)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Alarm Manager for Python logs over TCP Socket

To be done...

## Authors

* [**Carlos Vigo**](mailto:carlosv@phys.ethz.ch?subject=[GitLab%-%alarm-manager]) - *Initial work* - 
[GitLab](https://gitlab.ethz.ch/carlosv)

## Contributing

Please read our [contributing policy](CONTRIBUTING.md) for details on our code of
conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.ethz.ch/exotic-matter/cw-beam/alarm-manager).

## License

This project is licensed under the [GNU GPLv3 License](LICENSE.md)

## Built With

* [PyCharm Professional 2020](https://www.jetbrains.com/pycharm//) - The IDE used
* [Sphinx](https://www.sphinx-doc.org/en/master/index.html) - Documentation

## Acknowledgments

* Nobody so far
