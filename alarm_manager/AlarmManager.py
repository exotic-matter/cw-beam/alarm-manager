""" Daemon TCP server. The server will run indefinitely
listening on the specified TCP (see the
:class:`~lab_utils.socket_comm.Server` documentation).
When a client connects and sends a message string, the
message parser will call the appropriate method. The
following commands are supported by the parser (options
must be used with a double dash \\- \\-):

+-----------+-----------------------+---------------------------------------------------------------------------+
| quit      |                       | Stops the daemon and cleans up database and serial port                   |
+-----------+-----------------------+---------------------------------------------------------------------------+
| status    |                       | TODO: Not implemented yet                                                 |
+-----------+-----------------------+---------------------------------------------------------------------------+

"""

# Imports
from datetime import datetime

# Third party
from lab_utils.socket_comm import Server

# Local
from .__project__ import (
    __documentation__ as docs_url,
    __description__ as prog_desc,
    __module_name__ as mod_name,
)


class AlarmManager(Server):
    """ """

    # Attributes

    def __init__(self,
                 config_file: str = None,
                 pid_file_name: str = None,
                 host: str = None,
                 port: int = None):
        """ Initializes the :class:`Daemon` object.

        Parameters
        ----------
        config_file : str, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        pid_file_name : str, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        host : int, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        port : int, optional
            See parent class :class:`~lab_utils.socket_comm.Server`.

        Raises
        ------
        :class:`configparser.Error`
            Configuration file error

        :class:`LockError`
            The PID file could not be locked (see parent
            class :class:`~lab_utils.socket_comm.Server`).

        :class:`OSError`
            Socket errors (see parent class
            :class:`~lab_utils.socket_comm.Server`).

        """
        # Call the parent class initializer

        super().__init__(
            config_file=config_file,
            pid_file_name=pid_file_name,
            host=host,
            port=port,
        )

        # Add custom arguments to the message parser
        self.update_parser()

    def add_alarm_parser(
            self,
            name: str,
            description: str,
            func: object,
            which: str = None,
    ):
        if which is None:
            which = name

        sp = self.sp.add_parser(
            name=name,
            description=description,
        )
        sp.set_defaults(
            func=func,
            which=which
        )

        sp.add_argument(
            '--date',
            help='event date',
            dest='date',
            type=lambda s: datetime.strptime(s, '%Y-%m-%d %H:%M:%S'),
        )
        sp.add_argument(
            '--level',
            help='log level',
            dest='level',
        )
        sp.add_argument(
            '--app',
            help='faulty app',
            dest='app',
        )
        sp.add_argument(
            '--module',
            help='faulty module',
            dest='module',
        )
        sp.add_argument(
            '--function',
            help='faulty function',
            dest='function',
        )
        sp.add_argument(
            '--message',
            help='log message',
            dest='message',
        )

    def update_parser(self):
        """ Sets up the message
        :attr:`~lab_utils.socket_comm.Server.parser`. """

        self.logger.debug('Setting up custom message parser')

        # Set some properties of the base class argument parser
        self.parser.prog = mod_name
        self.parser.description = prog_desc
        self.parser.epilog = 'Check out the package documentation for more information:\n{}'.format(docs_url)

        # Subparsers for each acceptable command
        # 1. STATUS
        sp_status = self.sp.add_parser(
            name='status',
            description='checks the status of the daemon',
        )
        sp_status.set_defaults(
            func=self.status,
            which='status')

        # 2. TEST
        self.add_alarm_parser(
            name='socket_app',
            description='test function',
            func=self.test,
            which='test'
        )

    def quit(self):
        """ Stops the daemon, called with message 'quit'.
        The method overrides the original
        :meth:`~lab_utils.socket_comm.Server.quit` to do
        proper clean-up.
        """

        self.logger.info('Launching quitting sequence')

        self.logger.info('Stopping daemon TCP server now')
        self.reply += 'Stopping daemon TCP server now'

    def status(self):
        """ TODO
        """
        self.reply += 'Status: doing great!'

    def test(self):
        self.logger.debug('Alarm received on {date} from {app}/{module}: {level} {message}'.format(
            date=self.namespace.date,
            app=self.namespace.which,
            module=self.namespace.module,
            level=self.namespace.level,
            message=self.namespace.message,
        ))
