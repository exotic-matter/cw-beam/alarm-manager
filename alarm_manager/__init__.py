# -*- coding: utf-8 -*-
# Author: Carlos Vigo
# Contact: carlosv@phys.ethz.ch

""" Central management tool for alarm logs over TCP.
"""

# Local imports
from alarm_manager import __project__

__all__ = [
    __project__.__author__,
    __project__.__copyright__,
    __project__.__short_version__,
    __project__.__version__,
    __project__.__project_name__,
]
