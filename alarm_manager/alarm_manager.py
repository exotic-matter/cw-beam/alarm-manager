""" Alarm manager for TCP logs. The module launches an
:class:`~AlarmManager.AlarmManager` object which will
listen for alarm logs over a TCP socket."""

# Imports
from sys import argv
from configparser import Error as ConfigError
from argparse import ArgumentParser, Namespace

# Third party
from lab_utils.custom_logging import configure_logging, getLogger
from lab_utils.socket_comm import Client
from zc.lockfile import LockError

# Local packages
from alarm_manager.AlarmManager import AlarmManager
from alarm_manager.__project__ import (
    __documentation__ as docs_url,
    __module_name__ as module,
    __description__ as prog_desc,
)


def alarm_manager():
    """The main routine. It parses the input argument and acts accordingly.

    If the module is called with no argument or with the 'start' option,
    an :class:`~AlarmManager.AlarmManager` object will be started, provided
    one is not already running. Arguments two and three, if supplied, are the
    alarm and the logging configuration files respectively.

    If the module is called with any other argument besides 'start', a TCP
    message will be sent to the running instance of the alarm manager, and
    its reply will be printed."""

    # The argument parser
    ap = ArgumentParser(
        prog=module,
        description=prog_desc,
        add_help=True,
        epilog='Check out the package documentation for more information:\n{}'.format(docs_url)
    )

    # Optional arguments
    ap.add_argument(
        '-l',
        '--logging-config-file',
        help='configuration file with the logging options',
        default=None,
        dest='logging_config_file',
        type=str,
    )
    ap.add_argument(
        '-s',
        '--server-config-file',
        help='configuration file with the Alarm Manager options',
        default=None,
        dest='server_config_file',
        type=str,
    )
    ap.add_argument(
        '-a',
        '--address',
        help='host address',
        default=None,
        dest='host',
        type=str
    )
    ap.add_argument(
        '-p',
        '--port',
        help='host port',
        default=None,
        dest='port',
        type=int
    )

    # Main positional argument: control
    choices_list = [
        'run',
        'quit',
        'status',
    ]
    help_desc = 'starts, stops or checks the status of the Alarm Manager'
    ap.add_argument(
        dest='control',
        choices=choices_list,
        type=str,
        help=help_desc,
        default='status',
        nargs='?',
    )

    # Parse the arguments
    args: Namespace = ap.parse_args(args=argv[1:])

    # Setup logging
    configure_logging(
        config_file=args.logging_config_file,
        logger_name='Alarm Manager'
    )

    # Call appropriate function
    if args.control == 'run':
        run(args)
    else:
        send_message(args)


def send_message(args: Namespace):
    """ Sends a string message to a running
    :class:`AlarmManager` object over TCP."""

    try:
        # Start a client
        c = Client(
            config_file=args.server_config_file,
            host=args.host,
            port=args.port,
        )
        getLogger().info('Opening connection to the Alarm Manager server on {h}:{p}'.format(
            h=c.host,
            p=c.port
        ))

        # Send message and get reply
        getLogger().info('Sending message: %s', args.control)
        reply = c.send_message(args.control)
        getLogger().info(reply)

    except ConfigError as e:
        getLogger().error('Did you provide a valid configuration file?')

    except OSError as e:
        getLogger().error('Maybe the Alarm Manager server is not running, or running elsewhere')

    except BaseException as e:
        # Undefined exception, full traceback to be printed
        getLogger().exception("{}: {}".format(type(e).__name__, e))

    else:
        exit(0)
    exit(1)


def run(args: Namespace):
    """ Launches an :class:`AlarmManager` object."""

    try:
        # Start the daemon
        getLogger().info('Starting Alarm Manager server...')
        the_alarm_manager = AlarmManager(
            config_file=args.server_config_file,
            pid_file_name='/tmp/alarm_manager.pid',
            host=args.host,
            port=args.port,
        )
        the_alarm_manager.start_daemon()
        getLogger().info('Alarm Manager server stopped, bye!')

    except ConfigError as e:
        getLogger().error("{}: {}".format(type(e).__name__, e))
        getLogger().error('Did you provide a valid configuration file?')

    except OSError as e:
        getLogger().error("{}: {}".format(type(e).__name__, e))
        getLogger().error('Possible socket error, do you have permissions to the socket?')

    except LockError as e:
        getLogger().error("{}: {}".format(type(e).__name__, e))
        getLogger().error('Alarm Manager daemon is probably running elsewhere.')

    except BaseException as e:
        # Undefined exception, full traceback to be printed
        getLogger().exception("{}: {}".format(type(e).__name__, e))

    else:
        exit(0)

    exit(1)
